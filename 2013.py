from bs4 import BeautifulSoup
from urllib.parse import urljoin
from collections import namedtuple, defaultdict
from dataclasses import dataclass
from io import BytesIO
from lxml import etree
import Levenshtein
import pdfplumber
import requests_cache
import re

session = requests_cache.CachedSession('cache')

def get_soup(url):
    res = session.get(url)
    res.encoding = 'utf-8'
    return BeautifulSoup(res.text, 'lxml')

TIME_REGEX = r'\[(.+?)\] (.+)'
TOTAL_REGEX = r'(.+) \((.+)\)'
PROBLEM_REGEX = r'(.+)/(.+)'
PROBLEM_ALPHABET = 'abcdefghij'

@dataclass
class Problem:
    penalties: int
    ok: bool
    time_ok: int
    score: int
Score = namedtuple('Score', 'team, penalty_time, problems_solved')
Team = namedtuple('Team', 'key, site, institution, name, score')

def parse_boca_tr(tr, site):
    tds = [td.get_text().strip() for td in tr.select('td')]
    time_match = re.match(TIME_REGEX, tds[2])
    if time_match:
        institution = time_match[1]
        name = time_match[2].strip()
    else:
        institution = None
        name = tds[2].strip()
    total_match = re.match(TOTAL_REGEX, tds[3+len(PROBLEM_ALPHABET)])
    return Score(
        team=Team(
            key=tds[1].split('/')[0],
            site=site,
            institution=institution,
            name=name,
            score={
                key: Problem(
                    penalties=(penalties := int(problem_match[1]) if problem_match[2] == '-' else int(problem_match[1])-1),
                    ok=problem_match[2] != '-',
                    time_ok=(time_ok := 0 if problem_match[2] == '-' else int(problem_match[2])),
                    score=0 if time_ok == 0 else 20*penalties + time_ok,
                )
                for i, key in enumerate(PROBLEM_ALPHABET)
                if (problem_match := re.match(PROBLEM_REGEX, tds[3+i]))
            }
        ),
        problems_solved=int(total_match[1]),
        penalty_time=int(total_match[2]),
    )

def parse_boca_html(site, url):
    soup = get_soup(url)
    trs = soup.select(f'tr.sitegroup1')
    if len(trs) == 0: trs = soup.select('tr')[2:]
    return [
        parse_boca_tr(tr, site)
        for tr in trs if len(tr) > 3+len(PROBLEM_ALPHABET)
    ]

def build_scoreboard(site, tuples):
    scoreboard = []
    for key, name, score, given_problems_solved, given_penalty_time in tuples:
        scoreboard.append(Score(
            team=Team(key=key, site=site, institution=None, name=name, score=(built_score := {
                key: Problem(
                    penalties=(penalties := int(problem_match[1]) if (problem_match := re.match(PROBLEM_REGEX, cell))[2] == '-' else int(problem_match[1])-1),
                    ok=problem_match[2] != '-',
                    time_ok=(time_ok := 0 if problem_match[2] == '-' else int(problem_match[2])),
                    score=0 if time_ok == 0 else 20*penalties + time_ok,
                )
                for key, cell in score
            })),
            problems_solved=(problems_solved := sum(1 for v in built_score.values() if v.ok)),
            penalty_time=(penalty_time := sum(v.score for v in built_score.values() if v.ok)),
        ))
        assert(problems_solved == given_problems_solved)
        assert(penalty_time == given_penalty_time)
    return scoreboard

def parse_csus_tr(tr, site):
    tds = [td.get_text().strip() for td in tr.select('td')]
    rank, name, solved, time, *problems, total = tds
    return (
        name,
        name,
        [
            (problem_key, problem.replace('--', '-'))
            for problem_key, problem in zip(PROBLEM_ALPHABET, problems)
            if not problem.startswith('0')
        ],
        int(solved),
        int(time)
    )
def parse_csus_html(site, url):
    soup = get_soup(url)
    return build_scoreboard(site, [parse_csus_tr(tr, site) for tr in soup.select('tr')[2:-1]])

HUMAN_READING_PNGS = {
    'Balsas, MA': build_scoreboard('Balsas, MA', [
        ('8bits', '8 bits', [('a', '2/51'), ('e', '3/115')], 2, 226),
        ('owsla', 'Owsla', [('a', '1/12')], 1, 12),
        ('3gbi1bit', '3Gbi1Bit', [('a', '1/91')], 1, 91),
        ('mlgb', 'MLGB', [('a', '2/74'), ('e', '1/-')], 1, 94),
        ('mayor', 'Mayor', [('a', '1/151')], 1, 151),
        ('4system', '4System', [('a', '4/99')], 1, 159),
        ('dc', 'Devoradores de codigos', [('a', '5/85'), ('e', '1/-')], 1, 165),
        ('loadcode', 'load code', [('a', '6/126')], 1, 226),
        ('silvaesilva', 'Silva e Silva', [('a', '3/230'), ('e', '2/-')], 1, 270),
    ]),
    'Vitória da Conquista, BA': build_scoreboard('Vitória da Conquista, BA', [
        # note from a human: this does not reflect the picture, because they used 10 penalty time instead of 20
        ('zeroum', 'zeroum', [('a', '1/3'), ('e', '2/48'), ('f', '1/-')], 2, 71),
        ('chico', 'chico', [('a', '1/11'), ('e', '4/137'), ('f', '5/-')], 2, 208),
        ('thebits', 'The Bits', [('a', '1/15'), ('e', '4/208'), ('i', '1/-')], 2, 283),
        ('estricnina', 'estricnina', [('a', '1/4'), ('e', '5/-'), ('f', '2/-')], 1, 4),
        ('cangaceiros', 'cangaceiros digitais', [('a', '1/14'), ('e', '2/-'), ('f', '2/-')], 1, 14),
        ('naocompilou', 'nao compilou', [('a', '1/14')], 1, 14),
        ('main', 'main', [('a', '3/33'), ('e', '5/-')], 1, 73),
    ]),
    'Campinas, SP': build_scoreboard('Campinas, SP', [
        ('bobagi', '[USP] Ñtemtempabobagi',
            [('a', '1/2'), ('c', '3/77'), ('d', '2/155'), ('e', '1/6'), ('f', '2/37'), ('g', '1/94'), ('h', '3/158'), ('i', '1/50'), ('j', '1/204')], 9, 903),
        ('Triple', '[UNICAMP] I Triple Dare You',
            [('a', '1/2'), ('c', '2/126'), ('d', '4/191'), ('e', '1/13'), ('f', '2/49'), ('g', '1/85'), ('h', '2/147'), ('i', '1/51'), ('j', '2/172')], 9, 976),
        ('all', '[UNICAMP] All in',
            [('a', '1/7'), ('c', '1/65'), ('d', '1/172'), ('e', '1/11'), ('f', '2/61'), ('g', '1/96'), ('i', '1/128'), ('j', '4/256')], 8, 876),
        ('baoum', '[USP] aHHHH entaum tah bãoum',
            [('a', '1/3'), ('c', '14/277'), ('e', '2/34'), ('f', '4/104'), ('g', '5/281'), ('i', '1/133')], 6, 1252),
        ('balloons', '[UNICAMP] Plz sir, can we have balloons?',
            [('a', '1/7'), ('c', '1/111'), ('e', '2/20'), ('f', '1/61'), ('g', '2/214'), ('i', '1/-')], 5, 453),
        ('KernelPanic', '[USP] KernelPanic!',
            [('a', '1/8'), ('c', '1/222'), ('e', '1/14'), ('f', '2/199'), ('i', '2/109')], 5, 592),
        ('Deggub', '[UFSCar] Deggub',
            [('a', '1/5'), ('d', '1/135'), ('e', '3/34'), ('f', '1/125'), ('g', '4/-'), ('i', '1/270')], 5, 609),
        ('Trufinhas', '[PUC-Campinas] Trufinhas',
            [('a', '1/7'), ('e', '1/34'), ('f', '2/68'), ('g', '1/264')], 4, 393),
        ('Johnne', '[USP] O nome do meu time é Johnne',
            [('a', '1/9'), ('e', '1/14'), ('f', '1/115'), ('g', '11/286')], 4, 624),
        ('returntwo', '[UFSCar] returntwo',
            [('a', '1/6'), ('c', '6/-'), ('e', '1/21'), ('f', '1/71')], 3, 98),
        ('qualquer', '[UNICAMP] Qualquer nome serve',
            [('a', '1/11'), ('c', '2/-'), ('e', '3/63'), ('f', '1/63')], 3, 177),
        ('House', '[USP] Fredricksens House',
            [('a', '1/12'), ('e', '1/26'), ('f', '5/150')], 3, 268),
        ('Magela', '[PUC-Campinas] Magelas Team',
            [('a', '1/10'), ('e', '1/28'), ('f', '3/291')], 3, 369),
        ('mander', '[UNICAMP] Char mander',
            [('a', '1/7'), ('e', '1/11'), ('f', '3/-'), ('g', '3/-'), ('j', '6/-')], 2, 18),
        ('grafo', '[FAJ] Grafonautas',
            [('a', '1/9'), ('e', '1/28')], 2, 37),
        ('Developers', '[UNIARARAS] Developers',
            [('a', '1/14'), ('e', '1/30'), ('f', '2/-'), ('j', '8/-')], 2, 44),
        ('PUCampers', '[PUC-Campinas] PUCampers',
            [('a', '1/8'), ('e', '2/27'), ('f', '4/-'), ('g', '1/-')], 2, 55),
        ('poc', '[FAE] P.O.C.',
            [('a', '2/11'), ('e', '1/50')], 2, 81),
        ('Gama', '[UNIARARAS] Gama',
            [('a', '1/5'), ('e', '1/100'), ('f', '4/-'), ('g', '2/-')], 2, 105),
        ('first', '[UNIARARAS] first time',
            [('a', '1/16'), ('e', '3/50'), ('f', '3/-')], 2, 106),
        ('BubbleSort', '[PUC-Campinas] BubbleSort',
            [('a', '1/8'), ('e', '1/115'), ('f', '3/-')], 2, 123),
        ('porker', '[FAJ] Porker',
            [('a', '4/44'), ('e', '1/73'), ('f', '2/-')], 2, 177),
        ('pog', '[FAE] P.O.G.',
            [('a', '1/8'), ('e', '4/122'), ('f', '5/-')], 2, 190),
        ('Esquilos', '[UFSCar] Esquilos da Morte',
            [('a', '1/8'), ('c', '1/-'), ('d', '1/-'), ('e', '5/267'), ('f', '1/-')], 2, 355),
        ('the', '[PUC-Campinas] The Assemblers',
            [('a', '1/24'), ('d', '1/-'), ('e', '14/206')], 2, 490),
        ('Inda1', '[Anhaguera] Inda1',
            [('a', '3/78'), ('e', '9/226'), ('f', '5/-')], 2, 504),
        ('Cyber', '[FAJ] Cybertronic&amp;amp;amp;#39;s',
            [('a', '10/-')], 0, 0),
    ]),
    'Porto Velho, RO': build_scoreboard('Porto Velho, RO', [
        ('team6', 'Team Name', [('a', '1/20'), ('e', '1/43')], 2, 63),
        ('team3', 'Candiru', [('a', '1/20'), ('e', '1/62'), ('f', '2/-')], 2, 82),
        ('team12', 'RedZerg', [('a', '1/17'), ('c', '1/-'), ('e', '2/47')], 2, 84),
        ('team11', 'Potato Technological', [('a', '1/22'), ('e', '1/120')], 2, 142),
        ('team2', 'F2', [('a', '1/15'), ('e', '3/106')], 2, 161),
        ('team7', 'Dead_Lock', [('a', '1/79'), ('e', '2/124')], 2, 223),
        ('team8', 'Megalomaniacos', [('a', '1/29'), ('d', '1/-'), ('e', '1/206')], 2, 235),
        ('team5', 'Puraqué', [('a', '1/22'), ('e', '5/169')], 2, 271),
        ('team9', 'Ariquemes1', [('a', '1/22'), ('d', '1/-'), ('e', '9/-')], 1, 22),
        ('team1', 'F1', [('a', '1/88')], 1, 88),
        ('team10', 'Ariquemes2', [('a', '10/127')], 1, 307),
        ('team4', 'The men and a half girl', [('a', '4/-')], 0, 0),
    ]),
}
def check_with_humans(site, url):
    return HUMAN_READING_PNGS[site]

def parse_boca_pdf(site, url):
    tuples = []
    with pdfplumber.open(BytesIO(session.get(url).content)) as pdf:
        for i, page in enumerate(pdf.pages):
            tables = page.extract_tables()
            for table in tables:
                if len(table) == 1 and table[0][0] == 'BOCA': continue
                for row in table:
                    if row[0] is None: row = row[1:]
                    if 'BOCA' in row[0]: row = row[1:]
                    if row[0] == '#' or row[0] == '' or row[0] is None or not str.isdigit(row[0]): continue
                    if '(' in row[-2]:
                        row = row[:-2] + [row[-2] + row[-1]]
                    rank, key, name, *problems, total = row
                    key = key.removesuffix('/1')
                    name = name.replace('\n', ' ')
                    problems = [problem.replace('\xad', '-') for problem in problems]
                    total_match = re.match(TOTAL_REGEX, total)
                    tuples.append((key, key if name == '' else name, ((problem_key, problem) for problem_key, problem in zip(PROBLEM_ALPHABET, problems) if problem != ''), int(total_match[1]), int(total_match[2])))
    return build_scoreboard(site, tuples)

def parse_csus_txt(site, url):
    lines = session.get(url).content.splitlines()
    standings = False
    read = False
    for line in lines:
        if line == b'-- End XML --':
            read = False
        if read:
            xml += line
        if line == b'**** Standings XML  Report':
            standings = True
        if line == b'-- Start XML --' and standings:
            xml = b''
            read = True
    tuples = []
    root = etree.fromstring(xml)
    for el in root:
        if el.tag == 'teamStanding':
            problems = []
            for problem in el:
                index = int(problem.attrib['index'])-2
                if index < 0: continue
                attempts = int(problem.attrib['attempts'])
                solution_time = int(problem.attrib['solutionTime'])
                is_solved = problem.attrib['isSolved'] == 'true'
                if attempts:
                    problems.append((
                        chr(index-1+ord('a')),
                        f"{attempts}/{solution_time if is_solved else '-'}"))
            tuples.append((
                el.attrib["teamKey"],
                el.attrib["teamName"],
                problems,
                int(el.attrib["solved"]),
                int(el.attrib["points"]),
            ))
    return build_scoreboard(site, tuples)

from rich.table import Table
from rich import print
def print_sede_results(sede, scoreboard):
    table = Table(show_header=True)
    table.add_column("Time")
    for problem_key in PROBLEM_ALPHABET:
        table.add_column(problem_key.upper())
    table.add_column("Total")
    for row in scoreboard:
        table.add_row(
            row.team.name,
            *(
                f"{(problem := row.team.score[problem_key]).penalties + problem.ok}/{problem.time_ok if problem.ok else '-'}"
                if problem_key in row.team.score
                else ''
                for problem_key in PROBLEM_ALPHABET
            ),
            f'{row.problems_solved} ({row.penalty_time})'
        )
    print(table)

FIRST_PHASE_RESULT_URL = 'http://maratona.sbc.org.br/hist/2013/primeira-fase/'
SEDE_SCORE_PATH = {
    'Araçatuba, SP': ('Final_Detailed.htm', parse_boca_html),
    'Balsas, MA': ('ResultadoMaratona_Sede_Balsas.png', check_with_humans),
    'Belém, PA': ('relato%cc%81rio.txt', parse_csus_txt),
    'Campinas, SP': ('final_scoreboard_campinas.pdf', check_with_humans),
    'Chapecó, SC': ('detailedScoreboard.html', parse_boca_html),
    'Crato, CE': ('Detailed%20Scoreboard.pdf', parse_boca_pdf),
    'Cuiabá': ('detailscoreboard.html', parse_boca_html),
    'Feira de Santana, BA': ('Detailed%20Scoreboard.html', parse_boca_html),
    'Florianópolis, SC': ('placares_finais_florianopolis_2013/Detailed_scoreboard/Report%20Page.html', parse_boca_html),
    'Fortaleza, CE': ('score.html', parse_boca_html),
    'Goiânia, GO': ('DetailedScoreboard.html', parse_boca_html),
    'Itajubá, MG': ('Resultados-Itajuba/Detailed%20Scoreboard.pdf', parse_boca_pdf),
    'Juiz de Fora, MG': ('summary.html', parse_csus_html),
    'Londrina, PR': ('detailed%20scoreboard.htm', parse_boca_html),
    'Lorena, SP': ('score.php.html', parse_boca_html),
    'Marilia, SP': ('Detailed%20Scoreboard.pdf', parse_boca_pdf),
    'Montes Claros, MG': ('score.php.htm', parse_boca_html),
    'Nova Andradina, MS': ('Maratona%20de%20Programac%cc%a7a%cc%83o/Detailed%20Scoreboard.htm', parse_boca_html),
    'Olinda, PE': ('Final%20Scoreboard%20-%20Detalhado.pdf', parse_boca_pdf),
    'Palmas, TO': ('relatorio_Palmas/Report%20Page.html', parse_boca_html),
    'Patos, PB': ('Resultados%20Maratona%20-%20Patos%20PB/Resultados%20Maratona/Report%20Page.htm', parse_boca_html),
    'Pelotas, RS': ('report_boca_UFPel/detailed_score.html', parse_boca_html),
    'Ponta Grossa, PR': ('DetailedScore.html', parse_boca_html),
    'Porto Velho, RO': ('Captura%20de%20tela%20de%202013-09-14%2018_24_19.png', check_with_humans),
    'Salvador, BA': ('Detailed%20Scoreboard.html', parse_boca_html),
    'Santa Cruz do Sul, RS': ('Report%20Page%20Final%20Scoreboard.html', parse_boca_html),
    'Santos, SP': ('score-santos.pdf', parse_boca_pdf),
    'São Cristovão, SE': ('Final%20Scoreboard.html', parse_boca_html),
    'São Paulo, SP' : ('Report%20Page.pdf', parse_boca_pdf),
    'Sobral, CE': ('Enviar/score.html', parse_boca_html),
    'Teresina, PI': ('Teresina_Uespi_Detailed_Scoreboard.html', parse_boca_html),
    'Sorocaba, SP': ('ScoreSorocaba.pdf', parse_boca_pdf),
    'Teresópolis, RJ': ('PlacarTeresopolisCorrigido.pdf', parse_boca_pdf),
    'Uberlândia, MG': ('detailed_score.html', parse_boca_html),
    'Vitória, ES': ('Scoreboard.html', parse_boca_html),
    'Vitória da Conquista, BA': ('resultado%20oficial.png', check_with_humans),
}

by_problem = defaultdict(int)
final_by_problem = defaultdict(int)

soup = get_soup(FIRST_PHASE_RESULT_URL)
for row in soup.select('table[border="3d"] tr'):
    sede, r1, r2, r3, times = row.select('td')
    sede = sede.text.strip()
    if sede == 'Sede':
        # header
        continue
    final_times = [b.text.strip() for b in times.select('b')]
    results_a = times.select_one('a')
    if results_a:
        results_url = urljoin(FIRST_PHASE_RESULT_URL, results_a['href'])
        results_soup = get_soup(results_url)
        if sede in SEDE_SCORE_PATH:
            sede_score_path, parse = SEDE_SCORE_PATH[sede]
            while '/' in sede_score_path:
                subdir_path, sede_score_path = sede_score_path.split('/', 1)
                if subdir_a := results_soup.select_one(f'a[href="{subdir_path}/"]'):
                    results_url = urljoin(results_url + '/', subdir_a['href'])
                    results_soup = get_soup(results_url)
            scoreboard_a = results_soup.select_one(f'a[href="{sede_score_path}"]')
            if scoreboard_a:
                results_url = urljoin(results_url + '/', scoreboard_a["href"])
                scoreboard = parse(sede, results_url)
                print(sede)
                print_sede_results(sede, scoreboard)
                final_times_canonical = set()
                for j, final_time in enumerate(final_times):
                    if 'PUC-Rio' in final_time: final_time += 'PUC-Rio'
                    best = float('inf')
                    for i, score in enumerate(scoreboard):
                        d = Levenshtein.distance(score.team.name.lower(), final_time.lower())
                        if d < best:
                            best = d
                            best_name = score.team.name
                    final_times_canonical.add(best_name)
                for score in scoreboard:
                    for problem_key, problem in score.team.score.items():
                        by_problem[problem_key] += problem.ok
                        if score.team.name in final_times_canonical:
                            final_by_problem[problem_key] += problem.ok
            else:
                print(f'{sede}: Link not found')
        else:
            print(f'{sede}: Unknown sede')
    else:
        print(f'{sede}: Missing link')

table = Table('Problema', 'Acertos por times finalistas', 'Acertos totais')
for problem_key, amount in sorted(by_problem.items()):
    table.add_row(problem_key.upper(), str(final_by_problem[problem_key]), str(amount))
print(table)
