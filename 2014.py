from bs4 import BeautifulSoup
from urllib.parse import urljoin
from collections import namedtuple, defaultdict
from dataclasses import dataclass
from io import BytesIO
from lxml import etree
import Levenshtein
import pdfplumber
import requests_cache
import re

session = requests_cache.CachedSession('cache')

def get_soup(url):
    res = session.get(url)
    res.encoding = 'utf-8'
    return BeautifulSoup(res.text, 'lxml')

TIME_REGEX = r'[\[(](.+?)[\])] ?(.+)'
TOTAL_REGEX = r'(.+) \((.+)\)'
PROBLEM_REGEX = r'(.+)/(.+)'
PROBLEM_ALPHABET = 'abcdefghijk'

@dataclass
class Problem:
    penalties: int
    ok: bool
    time_ok: int
    score: int
Score = namedtuple('Score', 'team, penalty_time, problems_solved')
Team = namedtuple('Team', 'key, site, institution, name, score')

def parse_boca_tr(tr, site):
    tds = [td.get_text().strip() for td in tr.select('td')]
    time_match = re.match(TIME_REGEX, tds[2])
    if time_match:
        institution = time_match[1]
        name = time_match[2].strip()
    else:
        institution = None
        name = tds[2].strip()
    if name == '': name = tds[1].strip()
    total_match = re.match(TOTAL_REGEX, tds[3+len(PROBLEM_ALPHABET)])
    return Score(
        team=Team(
            key=tds[1].split('/')[0],
            site=site,
            institution=institution,
            name=name,
            score={
                key: Problem(
                    penalties=(penalties := int(problem_match[1]) if problem_match[2] == '-' else int(problem_match[1])-1),
                    ok=problem_match[2] != '-',
                    time_ok=(time_ok := 0 if problem_match[2] == '-' else int(problem_match[2])),
                    score=0 if time_ok == 0 else 20*penalties + time_ok,
                )
                for i, key in enumerate(PROBLEM_ALPHABET)
                if (problem_match := re.match(PROBLEM_REGEX, tds[3+i]))
            }
        ),
        problems_solved=int(total_match[1]),
        penalty_time=int(total_match[2]),
    )

def parse_boca_html(site, url):
    soup = get_soup(url)
    trs = soup.select(f'tr.sitegroup1')
    if len(trs) == 0: trs = soup.select('tr')[2:]
    if trs[0].find('td').text == '#': trs = trs[1:]
    return [
        parse_boca_tr(tr, site)
        for tr in trs if len(tr) > 3+len(PROBLEM_ALPHABET)
    ]

def build_scoreboard(site, tuples):
    scoreboard = []
    for key, name, score, given_problems_solved, given_penalty_time in tuples:
        scoreboard.append(Score(
            team=Team(key=key, site=site, institution=None, name=name, score=(built_score := {
                key: Problem(
                    penalties=(penalties := int(problem_match[1]) if (problem_match := re.match(PROBLEM_REGEX, cell))[2] == '-' else int(problem_match[1])-1),
                    ok=problem_match[2] != '-',
                    time_ok=(time_ok := 0 if problem_match[2] == '-' else int(problem_match[2])),
                    score=0 if time_ok == 0 else 20*penalties + time_ok,
                )
                for key, cell in score
            })),
            problems_solved=(problems_solved := sum(1 for v in built_score.values() if v.ok)),
            penalty_time=(penalty_time := sum(v.score for v in built_score.values() if v.ok)),
        ))
        assert(problems_solved == given_problems_solved)
        assert(penalty_time == given_penalty_time)
    return scoreboard

def parse_csus_tr(tr, site):
    tds = [td.get_text().strip() for td in tr.select('td')]
    rank, name, solved, time, *problems, total = tds
    return (
        name,
        name,
        [
            (problem_key, problem.replace('--', '-'))
            for problem_key, problem in zip(PROBLEM_ALPHABET, problems)
            if not problem.startswith('0')
        ],
        int(solved),
        int(time)
    )
def parse_csus_html(site, url):
    soup = get_soup(url)
    return build_scoreboard(site, [parse_csus_tr(tr, site) for tr in soup.select('tr')[2:-3]])

HUMAN_READING_PNGS = {
     'Balsas, MA': build_scoreboard('Balsas, MA', [
         ('vamoquevamo', '#VamoqueVamo', [('a', '1/16'), ('h', '1/23'), ('k', '1/204')], 3, 243),
         ('fotnog', 'FortNog', [('a', '1/73')], 1, 73),
         ('bombermans', 'BomberMans', [('a', '3/248')], 1, 288),
         ('brothers', 'Brothers', [('a', '5/280')], 1, 360),
         ('titas', 'Titãs', [], 0, 0),
     ]),
     'Crato, CE': build_scoreboard('Crato, CE', [
         ('icebug', 'Ice Bug Challenge', [('a', '1/58'), ('b', '1/132'), ('h', '1/34'), ('k', '3/-')], 3, 224),
         ('ifcex', 'IFCException', [('a', '1/41'), ('b', '1/184'), ('h', '1/30'), ('k', '2/-')], 3, 255),
         ('zueira', '#include zueira.h', [('a', '1/99'), ('h', '1/111'), ('k', '2/-')], 2, 210),
         ('miauz', 'MIAUZ', [('a', '1/40'), ('h', '4/208'), ('k', '3/-')], 2, 308),
     ]),
     'Parnaíba, PI': build_scoreboard('Parnaíba, PI', [
         ('mayllon', 'Mayllon Veras', [('a', '1/23'), ('b', '1/299'), ('g', '1/219'), ('h', '1/89'), ('k', '1/156')], 5, 786),
         ('piratas', 'Os Piratas', [('a', '1/128'), ('b', '1/-'), ('h', '1/34'), ('k', '1/101')], 3, 263),
         ('tesseract', 'Tesseract', [('a', '2/26'), ('b', '1/180'), ('h', '2/49')], 3, 295),
         ('winner', 'Winner is Coming', [('h', '1/17'), ('k', '1/57')], 2, 74),
         ('cec', 'C || !C', [('a', '1/110'), ('b', '2/-'), ('h', '1/16'), ('k', '1/-')], 2, 126),
     ]),
     'Porto Velho, RO': build_scoreboard('Porto Velho, RO', [
         # note from a human: there are two screen captures available, one is delayed by 300 minutes, we use the one that is not
         ('team01', 'Garr0sh', [('a', '1/12'), ('b', '1/-'), ('h', '1/27')], 2, 39),
         ('team04', 'Candiru', [('a', '1/17'), ('b', '2/-'), ('h', '1/50')], 2, 67),
         ('team07', 'Pégasus', [('a', '1/80'), ('b', '2/-'), ('h', '1/46'), ('k', '4/-')], 2, 126),
         ('team02', 'kaneda', [('a', '1/76'), ('h', '2/163'), ('k', '7/-')], 2, 259),
         ('team05', 'Javai C#edo', [('a', '2/168'), ('h', '1/199'), ('k', '1/-')], 2, 387),
         ('team05', 'Maresia', [('h', '2/204')], 1, 224),
         ('team03', 'Silicon Valley', [], 0, 0),
         ('team08', 'convidado', [('a', '1/-'), ('h', '1/-')], 0, 0),
     ]),
}
def check_with_humans(site, url):
    return HUMAN_READING_PNGS[site]

def parse_boca_runs_pdf(site, url):
    runs = []
    with pdfplumber.open(BytesIO(session.get(url).content)) as pdf:
        for i, page in enumerate(pdf.pages):
            tables = page.extract_tables()
            for table in tables:
                if len(table) == 1 and table[0][0] == 'BOCA': continue
                if len(table) == 3 and table[0][0] is None: continue
                if len(table) == 2 and table[0][0] == 'Users': continue
                if len(table) == 2 and table[0][0] == 'Problems': continue
                if len(table) == 2 and table[0][0] == 'Export': continue
                for row in table:
                    if row[0] is None or row[0] == '': row = row[1:]
                    if row[0] == '#' or row[0] == '' or row[0] is None: continue
                    if row[0] == 'Run #': continue
                    if row[1] is None: continue
                    no, site, user, time, problem, language, filename, status, judge, answer, *whatever = row
                    problem_index = problem[0].lower()
                    runs.append((int(no), user, time, problem_index, answer))
    scoreboard_by_team = defaultdict(lambda: defaultdict(dict))
    for no, user, time, problem_index, answer in sorted(runs, key=lambda x: x[0]):
        if problem_index not in scoreboard_by_team[user]:
            scoreboard_by_team[user][problem_index] = (answer == 'YES', time, 1)
        else:
            if scoreboard_by_team[user][problem_index][0] == True:
                print('Skipping, team already passed problem')
                continue
            attempts = scoreboard_by_team[user][problem_index][2]
            scoreboard_by_team[user][problem_index] = (answer == 'YES', time, attempts+1)
    tuples = []
    for team, problems in scoreboard_by_team.items():
        solved = sum(ok for ok, time, attemps in problems.values())
        total = sum(int(time) + 20*(attempts-1) for ok, time, attempts in problems.values() if ok)
        tuples.append((team, team, [(index, f'{attempts}/{time if ok else "-"}') for index, (ok, time, attempts) in problems.items()], solved, total))
    tuples = sorted(tuples, key=lambda x: (-x[3], x[4]))
    return build_scoreboard(site, tuples)

def parse_boca_pdf(site, url):
    tuples = []
    with pdfplumber.open(BytesIO(session.get(url).content)) as pdf:
        for i, page in enumerate(pdf.pages):
            tables = page.extract_tables()
            for table in tables:
                if len(table) == 1 and table[0][0] == 'BOCA': continue
                for row in table:
                    if row[0] is None: row = row[1:]
                    if 'BOCA' in row[0]: row = row[1:]
                    if len(row) > 1 and row[1] is not None and str.isdigit(row[1]): row = row = row[1:]
                    if row[0] == '#' or row[0] == '' or row[0] is None or not str.isdigit(row[0]): continue
                    if '(' in row[-2]:
                        row = row[:-2] + [row[-2] + row[-1]]
                    rank, key, name, *problems, total = row
                    key = key.removesuffix('/1')
                    name = name.replace('\n', ' ')
                    problems = [problem.replace('\xad', '-') for problem in problems]
                    total_match = re.match(TOTAL_REGEX, total)
                    tuples.append((key, key if name == '' else name, ((problem_key, problem) for problem_key, problem in zip(PROBLEM_ALPHABET, problems) if problem != ''), int(total_match[1]), int(total_match[2])))
    return build_scoreboard(site, tuples)

def parse_csus_txt(site, url):
    lines = session.get(url).content.splitlines()
    standings = False
    read = False
    for line in lines:
        if line == b'-- End XML --':
            read = False
        if read:
            xml += line
        if line == b'**** Standings XML  Report':
            standings = True
        if line == b'-- Start XML --' and standings:
            xml = b''
            read = True
    tuples = []
    root = etree.fromstring(xml)
    for el in root:
        if el.tag == 'teamStanding':
            problems = []
            for problem in el:
                index = int(problem.attrib['index'])-2
                if index < 0: continue
                attempts = int(problem.attrib['attempts'])
                solution_time = int(problem.attrib['solutionTime'])
                is_solved = problem.attrib['isSolved'] == 'true'
                if attempts:
                    problems.append((
                        chr(index-1+ord('a')),
                        f"{attempts}/{solution_time if is_solved else '-'}"))
            tuples.append((
                el.attrib["teamKey"],
                el.attrib["teamName"],
                problems,
                int(el.attrib["solved"]),
                int(el.attrib["points"]),
            ))
    return build_scoreboard(site, tuples)

from rich.table import Table
from rich import print
def print_sede_results(sede, scoreboard):
    table = Table(show_header=True)
    table.add_column("Time")
    for problem_key in PROBLEM_ALPHABET:
        table.add_column(problem_key.upper())
    table.add_column("Total")
    for row in scoreboard:
        table.add_row(
            row.team.name,
            *(
                f"{(problem := row.team.score[problem_key]).penalties + problem.ok}/{problem.time_ok if problem.ok else '-'}"
                if problem_key in row.team.score
                else ''
                for problem_key in PROBLEM_ALPHABET
            ),
            f'{row.problems_solved} ({row.penalty_time})'
        )
    print(table)

FIRST_PHASE_RESULT_URL = 'http://maratona.sbc.org.br/hist/2014/primeira-fase/'
SEDE_SCORE_PATH = {
    'Araçatuba, SP': ('Final%20Scoreboard.html', parse_boca_html),
    'Balsas, MA': ('Final%20score.png', check_with_humans),
    'Belém, PA': ('resultado-Belem-PA-13-09-2014.html', parse_csus_html),
    'Boa Vista, RR': ('detailed%20score%20-%20Boa%20Vista-RR.htm', parse_boca_html),
    'Campinas, SP': ('CAMPINAS_Final_Scoreboard.htm', parse_boca_html),
    'Campo Grande, MS': ('Final2.pdf', parse_boca_pdf),
    'Cascavel, PR': ('DetailedScoreboard.html', parse_boca_html),
    'Chapecó, SC': ('final-scoreboard.html', parse_boca_html),
    'Crato, CE': ('placarIMAGEM.png', check_with_humans),
    'Cuiabá': ('score.html', parse_boca_html),
    'Curitiba, PR': ('DetailedScoreBoard_Curitiba_PR_OK/Report%20Page.html', parse_boca_html),
    'Feira de Santana, BA': ('Detailed%20Score%20-%20Feira%20de%20Santana.html', parse_boca_html),
    'Fortaleza, CE': ('ScorePage.pdf', parse_boca_pdf),
    'Goiânia, GO': ('DetailedScoreboard.html', parse_boca_html),
    'Itajubá, MG': ('Detailed%20Scoreboard.pdf', parse_boca_pdf),
    'Juazeiro, BA': ('Report%20Page%20ScoreBoard.pdf', parse_boca_pdf),
    'Juiz de Fora, MG': ('FinalScoreBoard.htm', parse_boca_html),
    'Macapá, AP': ('detailedscoreboard.pdf', parse_boca_pdf),
    'Manaus, AM': ('FinalScore-Sede-MANAUS.html', parse_boca_html),
    'Marilia, SP': ('RunList.pdf', parse_boca_runs_pdf),
    'Montes Claros, MG': ('deitaledScaoreboard.html', parse_boca_html),
    'Natal, RN': ('RunListInNatal.pdf', parse_boca_runs_pdf),
    'Olinda, PE': ('score-detalhado.pdf', parse_boca_pdf),
    'Parnaíba, PI': ('20140913_193516.jpg', check_with_humans),
    'Porto Velho, RO': ('Captura%20de%20tela%20de%202014-09-13%2018_38_16.png', check_with_humans),
    'Rio de Janeiro, RJ': ('detailedscoreboard.html', parse_boca_html),
    'Rio Grande, RS': ('Report%20Page.html', parse_boca_html),
    'Salvador, BA': ('score.html', parse_boca_html),
    'Santa Cruz do Sul, RS': ('Admin\'s%20Page_score.html', parse_boca_html),
    'Santos, SP': ('score-santos.pdf', parse_boca_pdf),
    'São Cristovão, SE': ('FinalScoreBoard.html', parse_boca_html),
    'São José dos Campos, SP': ('DScore.htm', parse_boca_html),
    'São Luis, MA': ('RESULTADO_UFMA.htm', parse_boca_html),
    'São Paulo, SP': ('DetailedScoreboard.pdf', parse_boca_pdf),
    'Sorocaba, SP': ('scoreReport%20Page.pdf', parse_boca_pdf),
    'Teresina, PI': ('score.pdf', parse_boca_pdf),
    'Timoteo, MG': ('DetailScore.html', parse_boca_html),
    'Três de Maio, RS': ('Report%20Page.htm', parse_boca_html),
    'Uberlândia, MG': ('score_detail.html', parse_boca_html),
    'Vitória, ES': ('runs.pdf', parse_boca_runs_pdf),
    'Vitória da Conquista, BA': ('resultado_FInal_VDC.pdf', parse_boca_pdf),
}

by_problem = defaultdict(int)
final_by_problem = defaultdict(int)

soup = get_soup(FIRST_PHASE_RESULT_URL)
for row in soup.select('table[border="3d"] tr'):
    sede, r1, r2, r3, times = row.select('td')
    sede = sede.text.strip()
    if sede == 'Sede':
        # header
        continue
    final_times = [b.text.strip() for b in times.select('b') if b.parent.name != 'del']
    results_a = times.select_one('a')
    if results_a:
        results_url = urljoin(FIRST_PHASE_RESULT_URL, results_a['href'])
        results_soup = get_soup(results_url)
        if sede in SEDE_SCORE_PATH:
            sede_score_path, parse = SEDE_SCORE_PATH[sede]
            while '/' in sede_score_path:
                subdir_path, sede_score_path = sede_score_path.split('/', 1)
                if subdir_a := results_soup.select_one(f'a[href="{subdir_path}/"]'):
                    results_url = urljoin(results_url + '/', subdir_a['href'])
                    results_soup = get_soup(results_url)
            scoreboard_a = results_soup.select_one(f'a[href="{sede_score_path}"]')
            if scoreboard_a:
                results_url = urljoin(results_url + '/', scoreboard_a["href"])
                scoreboard = parse(sede, results_url)
                print(sede)
                print_sede_results(sede, scoreboard)
                final_times_canonical = set()
                for j, final_time in enumerate(final_times):
                    if final_time == 'UESB - Chicó':
                        final_time = 'Henrique, Charles e Isadora'
                    if final_time == 'UNIFESP - Trio de 11':
                        final_time = 'Trio de 11 (UNIFESP)'
                    if final_time == 'IFNMG - Segmentation Fault':
                        final_time = 'Sliva Endi, Oliva Luciano, Ribeiro Lucas'
                    if final_time == 'UFC - High Density Matter':
                        final_time = '[UFC 1] Universidade Federal do Ceara - High Density Matter'
                    if final_time == 'UFPR - Alwaysgood\'s team':
                        final_time = 'Alwaysgood&amp;#39;s team'
                    if final_time == 'CESUPA - MARRAPÁ':
                        final_time = 'Marrapa (CESUPA)'
                    best = float('inf')
                    for i, score in enumerate(scoreboard):
                        d = Levenshtein.distance(score.team.name.lower(), final_time.lower())
                        if d < best:
                            best = d
                            best_name = score.team.name
                    final_times_canonical.add(best_name)
                for score in scoreboard:
                    for problem_key, problem in score.team.score.items():
                        by_problem[problem_key] += problem.ok
                        if score.team.name in final_times_canonical:
                            final_by_problem[problem_key] += problem.ok
            else:
                print(f'{sede}: Link not found')
        else:
            print(f'{sede}: Unknown sede')
    else:
        print(f'{sede}: Missing link')

table = Table('Problema', 'Acertos por times finalistas', 'Acertos totais')
for problem_key, amount in sorted(by_problem.items()):
    table.add_row(problem_key.upper(), str(final_by_problem[problem_key]), str(amount))
print(table)
