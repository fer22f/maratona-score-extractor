from bs4 import BeautifulSoup
from urllib.parse import urljoin
from collections import namedtuple, defaultdict
from dataclasses import dataclass
from io import BytesIO
from lxml import etree
import Levenshtein
import pdfplumber
import requests_cache
import re

session = requests_cache.CachedSession('cache')

def get_soup(url):
    res = session.get(url)
    res.encoding = 'utf-8'
    return BeautifulSoup(res.text, 'lxml')

TIME_REGEX = r'[\[(](.+?)[\])] ?(.+)'
TOTAL_REGEX = r'(.+) \((.+)\)'
PROBLEM_REGEX = r'(.+)/(.+)'
PROBLEM_ALPHABET = 'abcdefghijkl'

@dataclass
class Problem:
    penalties: int
    ok: bool
    time_ok: int
    score: int
Score = namedtuple('Score', 'team, penalty_time, problems_solved')
Team = namedtuple('Team', 'key, site, institution, name, score')

def parse_boca_tr(tr, site):
    tds = [td.get_text().strip() for td in tr.select('td')]
    time_match = re.match(TIME_REGEX, tds[2])
    if time_match:
        institution = time_match[1]
        name = time_match[2].strip()
    else:
        institution = None
        name = tds[2].strip()
    if name == '': name = tds[1].strip()
    total_match = re.match(TOTAL_REGEX, tds[3+len(PROBLEM_ALPHABET)])
    return Score(
        team=Team(
            key=tds[1].split('/')[0],
            site=site,
            institution=institution,
            name=name,
            score={
                key: Problem(
                    penalties=(penalties := int(problem_match[1]) if problem_match[2] == '-' else int(problem_match[1])-1),
                    ok=problem_match[2] != '-',
                    time_ok=(time_ok := 0 if problem_match[2] == '-' else int(problem_match[2])),
                    score=0 if time_ok == 0 else 20*penalties + time_ok,
                )
                for i, key in enumerate(PROBLEM_ALPHABET)
                if (problem_match := re.match(PROBLEM_REGEX, tds[3+i]))
            }
        ),
        problems_solved=int(total_match[1]),
        penalty_time=int(total_match[2]),
    )

def parse_boca_html(site, url):
    soup = get_soup(url)
    trs = soup.select(f'tr.sitegroup1')
    if len(trs) == 0: trs = soup.select('tr')[2:]
    if trs[0].find('td').text == '#': trs = trs[1:]
    return [
        parse_boca_tr(tr, site)
        for tr in trs if len(tr) > 3+len(PROBLEM_ALPHABET)
    ]

def build_scoreboard(site, tuples):
    scoreboard = []
    for key, name, score, given_problems_solved, given_penalty_time in tuples:
        scoreboard.append(Score(
            team=Team(key=key, site=site, institution=None, name=name, score=(built_score := {
                key: Problem(
                    penalties=(penalties := int(problem_match[1]) if (problem_match := re.match(PROBLEM_REGEX, cell))[2] == '-' else int(problem_match[1])-1),
                    ok=problem_match[2] != '-',
                    time_ok=(time_ok := 0 if problem_match[2] == '-' else int(problem_match[2])),
                    score=0 if time_ok == 0 else 20*penalties + time_ok,
                )
                for key, cell in score
            })),
            problems_solved=(problems_solved := sum(1 for v in built_score.values() if v.ok)),
            penalty_time=(penalty_time := sum(v.score for v in built_score.values() if v.ok)),
        ))
        assert(problems_solved == given_problems_solved)
        assert(penalty_time == given_penalty_time)
    return scoreboard

def parse_csus_tr(tr, site):
    tds = [td.get_text().strip() for td in tr.select('td')]
    rank, name, solved, time, *problems, total = tds
    return (
        name,
        name,
        [
            (problem_key, problem.replace('--', '-'))
            for problem_key, problem in zip(PROBLEM_ALPHABET, problems)
            if not problem.startswith('0')
        ],
        int(solved),
        int(time)
    )
def parse_csus_html(site, url):
    soup = get_soup(url)
    return build_scoreboard(site, [parse_csus_tr(tr, site) for tr in soup.select('tr')[2:-3]])

HUMAN_READING_PNGS = {
     'Blumenau': build_scoreboard('Blumenau', [
        ('Blu71', 'Not Balde Samurai', [('a', '1/239'), ('b', '1/70'), ('c', '1/13'), ('d', '2/247'), ('e', '3/171'), ('f', '1/27'), ('j', '1/8')], 7, 835),
        ('Blu67', 'Pollos Hermanos', [('a', '3/238'), ('b', '4/108'), ('c', '1/2'), ('d', '1/214'), ('e', '2/219'), ('f', '1/28'), ('j', '1/18')], 7, 947),
        ('Blu76', 'POG', [('a', '1/259'), ('b', '1/203'), ('c', '2/8'), ('d', '2/298'), ('e', '2/224'), ('f', '1/22'), ('j', '1/14'), ('l', '1/-')], 7, 1088),
        ('Blu77', 'In Turing we trust!', [('b', '2/149'), ('c', '1/5'), ('d', '1/220'), ('e', '1/79'), ('f', '1/37'), ('j', '1/16')], 6, 526),
        ('Blu74', 'return void', [('c', '1/7'), ('e', '5/288'), ('f', '1/85'), ('j', '1/42')], 4, 502),
        ('Blu68', 'POXX', [('c', '1/5'), ('e', '4/-'), ('f', '1/14'), ('h', '1/-'), ('j', '1/33')], 3, 52),
        ('Blu85', 'Montinho Neles!', [('b', '1/-'), ('c', '1/7'), ('f', '1/31'), ('h', '1/-'), ('j', '1/45')], 3, 83),
        ('Blu87', 'X+2', [('c', '1/13'), ('f', '1/73'), ('j', '1/47')], 3, 133),
        ('Blu80', 'Osmar Contato Farofa', [('c', '1/6'), ('f', '2/85'), ('j', '1/34')], 3, 145),
        ('Blu69', 'The bug is on the table', [('b', '4/-'), ('c', '1/15'), ('f', '1/68'), ('i', '1/-'), ('j', '1/66')], 3, 149),
        ('Blu78', 'WithHopeWithoutHoppe', [('b', '1/-'), ('c', '1/24'), ('e', '2/-'), ('f', '1/52'), ('j', '3/33')], 3, 149),
        ('Blu86', 'Piratas do Vale do Itajai', [('b', '5/-'), ('c', '1/5'), ('f', '3/88'), ('j', '1/20')], 3, 153),
        ('Blu70', 'Debug\'s on the table', [('b', '2/-'), ('c', '1/17'), ('f', '2/95'), ('j', '1/44')], 3, 176),
        ('Blu81', 'SUSPIRO', [('c', '1/29'), ('f', '1/139'), ('j', '2/84')], 3, 272),
        ('Blu82', 'Priviet', [('b', '3/-'), ('c', '1/6'), ('f', '1/184'), ('j', '1/117')], 3, 307),
        ('Blu72', 'Janelas XP', [('c', '1/36'), ('e', '2/-'), ('f', '3/208'), ('g', '2/-'), ('j', '2/30')], 3, 334),
        ('Blu84', 'Gambosa', [('b', '4/-'), ('c', '1/10'), ('f', '1/82'), ('j', '5/232')], 3, 404),
        ('Blu73', 'Kode Krushers', [('c', '2/10'), ('j', '4/191'), ('l', '4/-')], 2, 281),
     ]),
     'Fortaleza': build_scoreboard('Fortaleza', [
         ('For161', 'Tha Ali Jom [Universidade Estadual do Ceara]', [
              ('a', '1/46'), ('b', '1/134'), ('c', '1/14'), ('d', '1/285'), ('e', '3/215'), ('f', '1/29'), ('h', '1/128'), ('j', '1/5'), ('k', '1/-')], 8, 896),
         ('For170', 'Java de C# e R [Universidade Estadual do Ceara]', [
              ('c', '1/14'), ('e', '4/147'), ('f', '2/32'), ('h', '1/230'), ('j', '1/16'), ('k', '1/272')], 6, 791),
         ('For171', 'O importante eh nao zerar [Instituto Federal de Educacao, Ciencia e T]', [
              ('c', '1/13'), ('e', '3/290'), ('f', '3/40'), ('j', '2/19')], 4, 462),
         ('For173', 'Jany [Universidade Estadual do Ceara]', [
              ('c', '1/20'), ('e', '5/210'), ('f', '2/133'), ('j', '1/76')], 4, 539),
         ('For155', 'Busain [Universidade Estadual do Ceara]', [
              ('c', '1/4'), ('e', '4/-'), ('f', '1/16'), ('g', '1/-'), ('h', '2/-'), ('j', '1/23')], 3, 43),
         ('For165', '2 ou + [Universidade Federal do Ceara - Campus Quixa]', [
              ('b', '1/-'), ('c', '1/14'), ('f', '1/24'), ('j', '1/11')], 3, 49),
         ('For159', 'An Unnamed Cell [Universidade Federal do Ceara - Campus Quixa]', [
              ('a', '3/-'), ('c', '1/3'), ('e', '2/-'), ('f', '1/38'), ('j', '1/17'), ('l', '2/-')], 3, 58),
         ('For174', 'E so usar map, Kevin [Universidade Estadual do Ceara]', [
              ('c', '1/10'), ('f', '1/55'), ('j', '1/26')], 3, 91),
         ('For158', '#UVA 1 [Universidade Estadual Vale do Acarau]', [
              ('c', '1/19'), ('e', '2/-'), ('f', '1/49'), ('g', '4/-'), ('j', '1/24')], 3, 92),
         ('For163', 'Cana Brava [Universidade Federal do Ceara]', [
              ('a', '1/-'), ('c', '1/10'), ('e', '1/-'), ('f', '2/45'), ('j', '1/18')], 3, 93),
         ('For156', 'Formata_que_resolve [Universidade Federal do Ceara - Campus Quixa]', [
              ('c', '1/4'), ('f', '2/64'), ('h', '2/-'), ('j', '1/14'), ('l', '1/-')], 3, 102),
         ('For164', 'Brain Juice [Universidade de Fortaleza]', [
              ('c', '1/28'), ('e', '1/-'), ('f', '1/59'), ('h', '1/-'), ('j', '1/25')], 3, 112),
         ('For167', 'Ainda estamos decidindo [Universidade Estadual do Ceara]', [
              ('b', '7/-'), ('c', '1/10'), ('f', '1/55'), ('j', '3/45')], 3, 150),
         ('For172', 'Os Profissionais e Eu [Universidade de Fortaleza]', [
              ('c', '1/23'), ('f', '1/76'), ('j', '1/53')], 3, 152),
         ('For168', 'Caqui [Universidade Fderal do Ceara]', [
              ('c', '1/9'), ('f', '1/48'), ('j', '3/68')], 3, 165),
         ('For169', 'The bug is real! [Universidade Federal do Ceara]', [
              ('c', '1/17'), ('e', '1/-'), ('f', '1/108'), ('j', '2/49')], 3, 194),
         ('For166', '#UVA 2 [Universidade Estadual Vale do Acarau]', [
              ('c', '1/28'), ('f', '1/117'), ('j', '1/56')], 3, 201),
         ('For162', 'Canoa Quebrada [Instituto Federal de Educacao, Ciencia e T]', [
              ('c', '1/14'), ('e', '2/-'), ('f', '1/210'), ('j', '2/129')], 3, 373),
         ('For160', '#UVA 3 [Universidade Estadual Vale do Acarau]', [
              ('c', '1/12'), ('f', '1/108'), ('j', '5/224')], 3, 424),
     ]),
     'São José dos Campos': build_scoreboard('São José dos Campos', [
         ('SJC473', 'ITA - Pgrnsue', [
              ('a', '1/174'), ('b', '1/29'), ('c', '1/17'), ('d', '1/71'), ('e', '2/105'), ('f', '1/34'), ('h', '1/135'), ('j', '1/8'), ('k', '2/195'), ('l', '5/217')], 10, 1105),
         ('SJC455', 'ITA - batata', [
              ('a', '1/190'), ('b', '3/65'), ('c', '1/6'), ('d', '1/159'), ('e', '1/172'), ('f', '2/39'), ('h', '1/197'), ('j', '1/5'), ('k', '3/269')], 9, 1202),
         ('SJC458', 'ITA - #define zuera us', [
              ('a', '2/67'), ('b', '1/156'), ('c', '1/13'), ('d', '1/179'), ('e', '2/262'), ('f', '1/71'), ('h', '1/201'), ('j', '1/9'), ('k', '2/232'), ('l', '1/-')], 9, 1250),
         ('SJC465', 'ITA - malluc ( )', [
              ('a', '1/116'), ('b', '1/232'), ('c', '1/5'), ('d', '1/168'), ('e', '2/125'), ('f', '1/35'), ('h', '1/248'), ('j', '1/19'), ('l', '41/-')], 8, 968),
         ('SJC466', 'UNITAU - Maldito Vo Pepe', [
              ('a', '3/94'), ('c', '1/9'), ('d', '2/218'), ('f', '1/27'), ('j', '1/7'), ('k', '2/103')], 6, 538),
         ('SJC456', 'FATEC - PAITOM', [
              ('a', '1/-'), ('b', '3/-'), ('c', '1/5'), ('d', '1/195'), ('f', '2/37'), ('j', '1/13')], 4, 270),
         ('SJC467', 'UNISAL - Achei que era Sao Silvestre', [
              ('b', '1/-'), ('c', '1/7'), ('e', '1/-'), ('f', '1/47'), ('j', '1/17')], 3, 71),
         ('SJC462', 'UNIFESP - Trio de 11', [
              ('b', '2/-'), ('c', '1/5'), ('e', '1/-'), ('f', '1/54'), ('j', '1/23')], 3, 82),
         ('SJC461', 'UNIVAP - 404 Member Not Found', [
              ('c', '1/17'), ('f', '2/108'), ('j', '1/12'), ('l', '1/-')], 3, 157),
         ('SJC454', 'FATEC Cruz. - FOUR EYES', [
              ('c', '1/22'), ('f', '1/169'), ('g', '1/-'), ('j', '1/46')], 3, 237),
         ('SJC470', 'FATEC - CodingB@d', [
              ('b', '1/-'), ('c', '1/18'), ('e', '1/-'), ('f', '2/117'), ('j', '2/98')], 3, 273),
         ('SJC460', 'UNIVAP - Los El\'s II', [
              ('c', '1/15'), ('f', '2/88'), ('j', '3/130')], 3, 293),
         ('SJC459', 'FATEC Cruz. - FATEKILA', [
              ('c', '1/17'), ('e', '1/-'), ('f', '1/267'), ('j', '1/38'), ('l', '1/-')], 3, 322),
         ('SJC471', 'UNISAL - Import RCP', [
              ('c', '1/56'), ('f', '1/195'), ('g', '2/-'), ('j', '1/85')], 3, 336),
         ('SJC463', 'FATEC Cruz. - X-CHARLES', [
              ('c', '1/17'), ('f', '1/215'), ('j', '2/111')], 3, 363),
         ('SJC464', 'UNISAL - Egg Panel', [
              ('c', '1/8'), ('f', '1/194'), ('j', '3/137')], 3, 379),
         ('SJC469', 'ANHANG. - Guaranis', [
              ('c', '1/50'), ('e', '1/-'), ('f', '3/154'), ('j', '3/103')], 3, 387),
         ('SJC368', 'UNIVAP - divergente', [
              ('c', '1/11'), ('f', '1/170'), ('j', '4/-')], 2, 181),
         ('SJC457', 'UNISAL - Challenge Accepted', [
              ('c', '1/25'), ('j', '5/-')], 1, 25),
     ]),
     'Serra': build_scoreboard('Serra', [
         ('taiti', 'Taiti', [('c', '1/11'), ('e', '3/241'), ('f', '1/61'), ('j', '1/41')], 4, 394),
         ('return0', 'Return 0', [('b', '3/-'), ('c', '1/5'), ('e', '2/144'), ('f', '3/57'), ('h', '3/-'), ('j', '5/87')], 4, 433),
         ('forkwf', 'fork while fork', [('a', '1/-'), ('c', '1/9'), ('e', '9/-'), ('f', '1/37'), ('j', '1/24')], 3, 70),
         ('fifostd', 'Padrão FIFO', [('c', '1/11'), ('e', '5/-'), ('f', '2/18'), ('j', '1/28')], 3, 77),
         ('burnbrain', 'Brun Brain', [('c', '1/21'), ('f', '1/49'), ('j', '1/18')], 3, 88),
         ('tle', 'TLE', [('a', '3/-'), ('c', '1/9'), ('e', '2/-'), ('f', '1/44'), ('j', '1/36'), ('l', '1/-')], 3, 89),
         ('binladen', 'bin/laden', [('c', '1/25'), ('e', '4/-'), ('f', '2/53'), ('j', '1/21')], 3, 119),
         ('lowlvl', 'low level men', [('b', '4/-'), ('c', '1/7'), ('e', '1/-'), ('f', '1/53'), ('j', '2/50')], 3, 130),
         ('freedel', 'Free(Del)', [('c', '1/20'), ('f', '1/83'), ('j', '1/32'), ('l', '1/-')], 3, 135),
         ('speedo', 'Speedo', [('c', '1/23'), ('e', '1/-'), ('f', '1/65'), ('j', '2/39')], 3, 147),
         ('coders', 'The Coders', [('c', '3/23'), ('e', '1/-'), ('f', '1/91'), ('j', '1/58')], 3, 212),
         ('gamb', 'Gambiarrados', [('c', '1/12'), ('f', '1/116'), ('j', '2/260')], 3, 408),
         ('giriz', 'Giriz', [('c', '1/21'), ('f', '3/-'), ('j', '1/51')], 2, 72),
     ]),
}
def check_with_humans(site, url):
    return HUMAN_READING_PNGS[site]

def parse_boca_runs_pdf(site, url):
    runs = []
    with pdfplumber.open(BytesIO(session.get(url).content)) as pdf:
        for i, page in enumerate(pdf.pages):
            tables = page.extract_tables()
            for table in tables:
                if len(table) == 1 and table[0][0] == 'BOCA': continue
                if len(table) == 3 and table[0][0] is None: continue
                if len(table) == 2 and table[0][0] == 'Users': continue
                if len(table) == 2 and table[0][0] == 'Problems': continue
                if len(table) == 2 and table[0][0] == 'Export': continue
                for row in table:
                    if row[0] is None or row[0] == '': row = row[1:]
                    if row[0] == '#' or row[0] == '' or row[0] is None: continue
                    if row[0] == 'Run #': continue
                    if row[1] is None: continue
                    if len(row) == 11: row = row[1:]
                    no, site, user, time, problem, language, filename, status, judge, answer, *whatever = row
                    problem_index = problem[0].lower()
                    runs.append((int(no), user, time, problem_index, answer))
    scoreboard_by_team = defaultdict(lambda: defaultdict(dict))
    for no, user, time, problem_index, answer in sorted(runs, key=lambda x: x[0]):
        if problem_index not in scoreboard_by_team[user]:
            scoreboard_by_team[user][problem_index] = (answer == 'YES', time, 1)
        else:
            if scoreboard_by_team[user][problem_index][0] == True:
                print('Skipping, team already passed problem')
                continue
            attempts = scoreboard_by_team[user][problem_index][2]
            scoreboard_by_team[user][problem_index] = (answer == 'YES', time, attempts+1)
    tuples = []
    for team, problems in scoreboard_by_team.items():
        solved = sum(ok for ok, time, attemps in problems.values())
        total = sum(int(time) + 20*(attempts-1) for ok, time, attempts in problems.values() if ok)
        tuples.append((team, team, [(index, f'{attempts}/{time if ok else "-"}') for index, (ok, time, attempts) in problems.items()], solved, total))
    tuples = sorted(tuples, key=lambda x: (-x[3], x[4]))
    return build_scoreboard(site, tuples)

ADD_ROW = {
    'MG-Uberlândia': [
        (20, ('teamulmg51', '[IFTM] Sistemas IFTM', [('a', '1/26'), ('d', '17/-'), ('h', '1/41'), ('j', '1/-')], 2, 67)),
        (45, ('teamulmg25', '[Pitagoras/SI] Experts Team', [('a', '9/204'), ('d', '1/-')], 1, 364)),
    ],
    'MG-Varginha': [
        (22, ('teamvamg24', '[INATEL] esquadrão suicida', [('a', '1/14'), ('d', '2/-'), ('h', '1/60')], 2, 74)),
    ],
    'PE-Olinda': [
        (11, ('teamolpe24', '[UF Pernambuco] Bugless', [('a', '1/4'), ('c', '2/256'), ('d', '11/-'), ('f', '2/79'), ('g', '1/-'), ('h', '1/9'), ('i', '3/32'), ('l', '1/95')], 6, 555)),
        (29, ('teamolpe5', '[FIBM] AESO 1', [('a', '1/34'), ('d', '2/-'), ('h', '4/158')], 2, 252)),
    ],
    'SP-Piracicaba': [
        (22, ('teampisp27', '[EEP] IDK', [('a', '1/33')], 1, 33))
    ],
    'SP-São Paulo': [
        (29, ('teamspsp1', '[UNINOVE] Uninove Beta', [('a', '1/23'), ('d', '11/-'), ('h', '3/120'), ('i', '1/-'), ('j', '1/-'), ('l', '3/-')], 2, 183)),
    ]
}
def parse_boca_pdf_and_add_row(site, url):
    scoreboard = parse_boca_pdf(site, url)
    for pos, row in ADD_ROW.get(site, []):
        scoreboard.insert(pos, build_scoreboard(site, [row])[0])
    return scoreboard

TOTAL_SUPPL = {
    'PR-Curitiba': [(7, 675), (7, 698), (6, 639), (5, 517), (5, 525), (5, 1143), (4, 524), (4, 646), (3, 206), (3, 301), (3, 341), (2, 104), (2, 203), (2, 357)]
}
def parse_boca_pdf(site, url):
    tuples = []
    with pdfplumber.open(BytesIO(session.get(url).content)) as pdf:
        for i, page in enumerate(pdf.pages):
            tables = page.extract_tables()
            for table in tables:
                if len(table) == 1 and table[0][0] == 'BOCA': continue
                for row in table:
                    if row[0] is None: row = row[1:]
                    if row[0] is None: row = row[1:]
                    if 'BOCA' in row[0]: row = row[1:]
                    if len(row) > 1 and row[1] is not None and str.isdigit(row[1]): row = row = row[1:]
                    if row[0] == '#' or row[0] == '' or row[0] is None or not str.isdigit(row[0]): continue
                    if '(' in row[-2] and row[-1] is None:
                        row = row[:-1]
                    if '(' in row[-2]:
                        row = row[:-2] + [row[-2] + row[-1]]
                    rank, key, name, *problems, total = row
                    key = key.removesuffix('/1')
                    name = name.replace('\n', ' ')
                    problems = [problem.replace('\xad', '-') for problem in problems if problem is not None]
                    if len(problems) > len(PROBLEM_ALPHABET) and site == 'Olinda':
                        problems = problems[1:]
                    total_match = re.match(TOTAL_REGEX, total)
                    if total_match is None and site in TOTAL_SUPPL:
                        total_match = ('', *TOTAL_SUPPL[site][int(rank)-1])
                    tuples.append((key, key if name == '' else name, ((problem_key, problem) for problem_key, problem in zip(PROBLEM_ALPHABET, problems) if problem != ''), int(total_match[1]), int(total_match[2])))
    return build_scoreboard(site, tuples)

def parse_csus_txt(site, url):
    lines = session.get(url).content.splitlines()
    standings = False
    read = False
    for line in lines:
        if line == b'-- End XML --':
            read = False
        if read:
            xml += line
        if line == b'**** Standings XML  Report':
            standings = True
        if line == b'-- Start XML --' and standings:
            xml = b''
            read = True
    tuples = []
    root = etree.fromstring(xml)
    for el in root:
        if el.tag == 'teamStanding':
            problems = []
            for problem in el:
                index = int(problem.attrib['index'])-2
                if index < 0: continue
                attempts = int(problem.attrib['attempts'])
                solution_time = int(problem.attrib['solutionTime'])
                is_solved = problem.attrib['isSolved'] == 'true'
                if attempts:
                    problems.append((
                        chr(index-1+ord('a')),
                        f"{attempts}/{solution_time if is_solved else '-'}"))
            tuples.append((
                el.attrib["teamKey"],
                el.attrib["teamName"],
                problems,
                int(el.attrib["solved"]),
                int(el.attrib["points"]),
            ))
    return build_scoreboard(site, tuples)

from rich.table import Table
from rich import print
def print_sede_results(sede, scoreboard):
    table = Table(show_header=True)
    table.add_column("Time")
    for problem_key in PROBLEM_ALPHABET:
        table.add_column(problem_key.upper())
    table.add_column("Total")
    for row in scoreboard:
        table.add_row(
            row.team.name,
            *(
                f"{(problem := row.team.score[problem_key]).penalties + problem.ok}/{problem.time_ok if problem.ok else '-'}"
                if problem_key in row.team.score
                else ''
                for problem_key in PROBLEM_ALPHABET
            ),
            f'{row.problems_solved} ({row.penalty_time})'
        )
    print(table)

FIRST_PHASE_RESULT_URL = 'http://maratona.sbc.org.br/hist/2016/primeira-fase/'

by_problem = defaultdict(int)
final_by_problem = defaultdict(int)

soup = get_soup(FIRST_PHASE_RESULT_URL)
for row in soup.select('table[border="3d"] tr'):
    sede, r1, r2, r3, times = row.select('td')
    sede = sede.text.strip()
    if sede == 'Sede': continue
    if sede == 'Total': continue
    final_times = [b.text.strip() for b in times.select('font')]
    if len(final_times) == 1: final_times = [x.strip() for x in final_times[0].split('\n')]
    results_a = times.select_one('a')
    if results_a:
        results_url = urljoin(FIRST_PHASE_RESULT_URL, results_a['href'])
        scoreboard = parse_boca_pdf_and_add_row(sede, results_url)
        print(sede)
        print_sede_results(sede, scoreboard)
        final_times_canonical = set()
        for j, final_time in enumerate(final_times):
            if final_time == 'Pitágoras 3':
                final_time = '[Pitágoras] Pitágoras 3'
            if final_time == 'UFPE - 0xE':
                final_time = '[UF Pernambuco] 0xE'
            if final_time == 'IFPB-JP - e-Coders':
                final_time = '[IFPB ­ João Pessoa] e­ Coders'
            if final_time == 'UNESP Bauru':
                final_time = '[UNESP­Bauru] UNESP Bauru'
            if final_time == 'UFS - PrÃ� q nome?':
                final_time = '[U Federal de Sergipe] Pra q nome?'
            best = float('inf')
            for i, score in enumerate(scoreboard):
                d = Levenshtein.distance(score.team.name.lower(), final_time.lower())
                if d < best:
                    best = d
                    best_name = score.team.name
            final_times_canonical.add(best_name)
        print(final_times, final_times_canonical)
        for score in scoreboard:
            for problem_key, problem in score.team.score.items():
                by_problem[problem_key] += problem.ok
                if score.team.name in final_times_canonical:
                    final_by_problem[problem_key] += problem.ok
    else:
        print(f'{sede}: Missing link')

table = Table('Problema', 'Acertos por times finalistas', 'Acertos totais')
for problem_key, amount in sorted(by_problem.items()):
    table.add_row(problem_key.upper(), str(final_by_problem[problem_key]), str(amount))
print(table)
